package test

import (
	"gunmurat7/todo-app-server/repository"
	"gunmurat7/todo-app-server/service"
	"testing"

	"github.com/stretchr/testify/assert"
)

var serv = service.NewTodoService(repository.NewMockRepository())

func TestGetTodosMongo(t *testing.T) {
	todos, err := serv.GetTodosMongo()

	assert.Equal(t, nil, err)
	assert.Equal(t, 2, len(todos))
}

func TestGetTodoMongo(t *testing.T) {

	todo, err := serv.GetTodoMongo("5e9d9c3a3f0f3c0f0c8b8f3e")

	assert.Equal(t, nil, err)
	assert.Equal(t, "5e9d9c3a3f0f3c0f0c8b8f3e", todo.ID.Hex())

}

func TestCreateTodoMongo(t *testing.T) {

	body := []byte(`{"title":"test","description":"test"}`)

	todo, err := serv.CreateTodoMongo(body)

	assert.Equal(t, nil, err)
	assert.Equal(t, "test", todo.Title)
	assert.Equal(t, false, todo.Completed)

}

func TestUpdateTodoMongo(t *testing.T) {

	todo, err := serv.GetTodoMongo("5e9d9c3a3f0f3c0f0c8b8f3e")

	assert.Equal(t, nil, err)
	assert.Equal(t, "5e9d9c3a3f0f3c0f0c8b8f3e", todo.ID.Hex())

	body := []byte(`{"title":"test"}`)

	uTodo, uerr := serv.UpdateTodoMongo("5e9d9c3a3f0f3c0f0c8b8f3e", body)

	assert.Equal(t, nil, uerr)
	assert.Equal(t, "test", uTodo.Title)

}

func TestDeleteTodoMongo(t *testing.T) {

	todo, err := serv.GetTodoMongo("5e9d9c3a3f0f3c0f0c8b8f3e")

	assert.Equal(t, nil, err)
	assert.Equal(t, "5e9d9c3a3f0f3c0f0c8b8f3e", todo.ID.Hex())

	dTodo, nerr := serv.DeleteTodoMongo("5e9d9c3a3f0f3c0f0c8b8f3e")

	assert.Equal(t, nil, nerr)
	assert.Equal(t, "", dTodo.Title)

}

func TestTodoUpdateCompleted(t *testing.T) {

	todo, err := serv.GetTodoMongo("5e9d9c3a3f0f3c0f0c8b8f3e")

	assert.Equal(t, nil, err)
	assert.Equal(t, "5e9d9c3a3f0f3c0f0c8b8f3e", todo.ID.Hex())

	uTodo, uerr := serv.UpdateTodoCompletedMongo("5e9d9c3a3f0f3c0f0c8b8f3e")

	assert.Equal(t, nil, uerr)
	assert.Equal(t, true, uTodo)

}
