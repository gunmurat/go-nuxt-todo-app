package test

import (
	"fmt"
	"gunmurat7/todo-app-server/helpers"
	"gunmurat7/todo-app-server/server"
	"log"
	"os"
	"testing"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/pact-foundation/pact-go/utils"
)

func startProvider(port string) {

	helpers.IsTestEnv = true

	go server.StartServer(port)

}

type Settings struct {
	Host            string
	ProviderName    string
	PactBrokerUrl   string
	BrokerToken     string // Bearer authentication
	ConsumerName    string
	ConsumerVersion string // a git sha, semantic version number
	ProviderVersion string
}

func (s *Settings) create() {
	s.Host = os.Getenv("HOST")
	s.ProviderName = os.Getenv("PROVIDER_NAME")
	s.ConsumerName = os.Getenv("CONSUMER_NAME")
	s.PactBrokerUrl = os.Getenv("PACT_BROKER_URL")
	s.BrokerToken = os.Getenv("PACT_BROKER_TOKEN")
	s.ProviderVersion = os.Getenv("PROVIDER_VERSION")
	s.ConsumerVersion = os.Getenv("CONSUMER_VERSION")
}

// func (s *Settings) create() {
// 	s.Host = "localhost"                             //"127.0.0.1"
// 	s.ProviderName = "TodoProvider"                  //"TodoProvider"
// 	s.ConsumerName = "TodoConsumer"                  //"TodoConsumer"
// 	s.PactBrokerUrl = "https://gunmurat.pactflow.io" //"http://localhost"
// 	s.BrokerToken = "cGx4suqZ3p-TFWWWk-lPEw"
// 	s.ProviderVersion = "1.0.1" //"1.0.0"
// 	s.ConsumerVersion = "1.0.0" //"1.0.0"
// }

func (s *Settings) getPactURL() string {
	var pactURL string

	if s.ConsumerVersion == "" {
		pactURL = fmt.Sprintf("%s/pacts/provider/%s/consumer/%s/latest/master.json", s.PactBrokerUrl, s.ProviderName, s.ConsumerName)
	} else {
		pactURL = fmt.Sprintf("%s/pacts/provider/%s/consumer/%s/version/%s.json", s.PactBrokerUrl, s.ProviderName, s.ConsumerName, s.ConsumerVersion)
	}

	return pactURL
}

func TestTodoPactProvider(t *testing.T) {

	port, portErr := utils.GetFreePort()
	if portErr != nil {
		log.Fatal(portErr)
	}

	startProvider(fmt.Sprintf(":%d", port))

	settings := Settings{}
	settings.create()

	pact := &dsl.Pact{
		Host:                     settings.Host,
		Provider:                 settings.ProviderName,
		Consumer:                 settings.ConsumerName,
		LogLevel:                 "DEBUG",
		LogDir:                   "logs",
		DisableToolValidityCheck: true,
	}

	verifyRequest := types.VerifyRequest{
		ProviderBaseURL:            fmt.Sprintf("http://%s:%d", settings.Host, port),
		ProviderVersion:            settings.ProviderVersion,
		BrokerURL:                  settings.PactBrokerUrl,
		BrokerToken:                settings.BrokerToken,
		PactURLs:                   []string{settings.getPactURL()},
		PublishVerificationResults: true,
		FailIfNoPactsFound:         true,
	}

	log.Printf("$PATH")

	verifyResponses, err := pact.VerifyProvider(t, verifyRequest)
	if err != nil {
		t.Fatalf("Error: %v", err)
	}

	fmt.Println(len(verifyResponses), "pact tests run")

}
