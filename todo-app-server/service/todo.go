package service

import (
	"encoding/json"
	"errors"
	"gunmurat7/todo-app-server/models"
	"gunmurat7/todo-app-server/repository"
	"time"
)

type TodoServiceInterface interface {
	GetTodosMongo() ([]models.Todo, error)
	GetTodoMongo(id string) (*models.Todo, error)
	CreateTodoMongo(body []byte) (*models.Todo, error)
	UpdateTodoMongo(id string, body []byte) (*models.Todo, error)
	DeleteTodoMongo(id string) (*models.Todo, error)
	UpdateTodoCompletedMongo(id string) (bool, error)
}

type TodoService struct {
	repository repository.TodoRepository
}

func NewTodoService(repo repository.TodoRepository) TodoServiceInterface {
	return &TodoService{
		repository: repo,
	}
}

func (service *TodoService) GetTodosMongo() ([]models.Todo, error) {
	res, err := service.repository.GetAllTodos()
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (service *TodoService) GetTodoMongo(id string) (*models.Todo, error) {
	res, err := service.repository.GetOneTodo(id)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (service *TodoService) CreateTodoMongo(body []byte) (*models.Todo, error) {
	var model models.Todo

	decErr := json.Unmarshal(body, &model)
	if decErr != nil {
		return nil, errors.New("todo can not decode")
	}

	model.CreatedAt = time.Now()

	res, err := service.repository.CreateTodo(&model)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (service *TodoService) UpdateTodoMongo(id string, body []byte) (*models.Todo, error) {

	todo, err := service.repository.GetOneTodo(id)
	if err != nil {
		return nil, err
	}

	decErr := json.Unmarshal(body, &todo)
	if decErr != nil {
		return nil, errors.New("todo can not decode")
	}

	res, err := service.repository.UpdateTodo(todo)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (service *TodoService) DeleteTodoMongo(id string) (*models.Todo, error) {

	_, err := service.repository.GetOneTodo(id)
	if err != nil {
		return nil, err
	}

	res, err := service.repository.DeleteTodo(id)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (service *TodoService) UpdateTodoCompletedMongo(id string) (bool, error) {
	res, err := service.repository.GetOneTodo(id)
	if err != nil {
		return false, err
	}

	res.Completed = !res.Completed

	_, uErr := service.repository.UpdateTodo(res)
	if uErr != nil {
		return false, uErr
	}

	return res.Completed, nil
}
