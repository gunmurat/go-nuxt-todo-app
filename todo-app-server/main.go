package main

import (
	"gunmurat7/todo-app-server/helpers"
	"gunmurat7/todo-app-server/server"
	"log"
	"os"
)

func init() {
	helpers.Initialize()
}

// export PATH=$PATH:/usr/local/go/bin:/Users/muratgun/go/bin

func main() {
	err := server.StartServer(os.Getenv("PORT"))
	if err != nil {
		log.Fatal(err)
	}
}
