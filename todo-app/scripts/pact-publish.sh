#!/bin/bash

VERSION=$1

curl -X PUT \
    https://gunmurat.pactflow.io/pacts/provider/TodoProvider/consumer/TodoConsumer/version/$VERSION \
    -H "Content-Type: application/json" \
    -d /pact/pacts/todoconsumer-todoprovider.json