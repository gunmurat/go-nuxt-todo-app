import todoService from '@/server/api/todo_service.js';

const state = () => ({
    todos: [],
});

const getters = {
    getTodos: state => state.todos,

    getTodoById: state => id => {
        return state.todos.find(todo => todo._id === id);
    },
};

const mutations = {
    SET_TODOS(state, todos) {
        state.todos = todos;
    },

    ADD_TODO(state, todo) {
        state.todos.push(todo);
    },

    UPDATE_TODO(state, todo) {
        const index = state.todos.findIndex(t => t._id === todo.id);
        state.todos.splice(index, 1, todo);
    },

    DELETE_TODO(state, id) {
        const index = state.todos.findIndex(t => t._id === id);
        state.todos.splice(index, 1);
    },

    UPDATE_COMPLETED(state, id) {
        const index = state.todos.findIndex(t => t._id === id);
        state.todos[index].completed = !state.todos[index].completed;
    },
};


const actions = {
    async fetchTodos({ commit }) {
        const todos = await todoService.getTodos();
        commit('SET_TODOS', todos.data);
    },

    async fetchSingleTodo({ commit }, id) {
        const todo = await todoService.getTodo(id);
        commit('ADD_TODO', todo.data);
    },

    async addTodo({ commit }, todo) {
        const newTodo = await todoService.createTodo(todo);
        commit('ADD_TODO', newTodo);
    },

    async updateTodo({ commit }, todo) {
        const updatedTodo = await todoService.updateTodo(todo._id, todo);
        commit('UPDATE_TODO', updatedTodo);
    },

    async deleteTodo({ commit }, id) {
        await todoService.deleteTodo(id);
        commit('DELETE_TODO', id);
    },

    async updateCompleted({ commit }, id) {
        await todoService.updateCompleted(id);
        commit('UPDATE_COMPLETED', id);
    },

};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};